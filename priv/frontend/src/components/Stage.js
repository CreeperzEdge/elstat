import React from 'react'
import PropTypes from 'prop-types'

import ms from 'ms'

export default function Stage({ stage }) {
  const { created_at: createdAt, title, content } = stage
  const ago = ms(Date.now() - createdAt * 1000)

  return (
    <div className="stage">
      <h3>{title}</h3>
      <p>{content}</p>
      <footer>{ago} ago</footer>
    </div>
  )
}

Stage.propTypes = {
  stage: PropTypes.shape({
    created_at: PropTypes.number,
    title: PropTypes.string,
    content: PropTypes.string,
  }),
}
