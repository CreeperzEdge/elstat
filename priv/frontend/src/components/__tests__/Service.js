import React from 'react'
import { shallow } from 'enzyme'

import Service, { getServiceState } from '../Service'

const graph = [[1000, 50], [2000, 30], [3000, 60]]

const props = {
  name: 'sample service',
  description: 'a cool service',
  latency: 50.5,
  uptime: 99.9994,
}

describe('<Service/>', () => {
  it('renders without crashing', () => {
    shallow(<Service graph={null} status {...props} />)
  })

  it('omits information', () => {
    const comp = shallow(
      <Service graph={null} status {...props} latency={null} />
    )
    expect(comp.find('h2.title').text()).toEqual('sample service ')
  })

  it('renders proper information', () => {
    const comp = shallow(<Service graph={graph} status {...props} />)
    expect(comp.prop('className')).toEqual('service service-alive')
    expect(comp.find('h2.title').text()).toEqual(
      'sample service 51ms (99.9994%)'
    )
    expect(
      comp.contains(<p className="description">a cool service</p>)
    ).toEqual(true)
    expect(
      comp.contains(<span className="information">51ms (99.9994%)</span>)
    ).toEqual(true)
  })
})

describe('getServiceState', () => {
  it('returns alive', () => {
    expect(getServiceState(true, 10, 5000)).toEqual('alive')
  })

  it('returns slow', () => {
    expect(getServiceState(true, 8000, 5000)).toEqual('slow')
  })

  it('returns dead', () => {
    expect(getServiceState(false, 0, 5000)).toEqual('dead')
  })
})
