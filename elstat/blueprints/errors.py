class ApiError(Exception):
    """Base API error class."""
    @property
    def message(self):
        """Get a message from the error"""
        return self.args[0]


class BadRequest(ApiError):
    """Represents a bad payload from the client."""
    status_code = 400


class Unauthorized(ApiError):
    """Authentication failure."""
    status_code = 401
